package currency_test

import (
	"encoding/json"
	"testing"

	"gitlab.com/henriquealexandre/books-organiser/currency"
)

func TestString(t *testing.T) {
	tests := []struct {
		name  string
		exp   string
		value currency.Currency
	}{
		{name: "zero", exp: "$0.00", value: currency.Currency(0)},
		{name: "2-cents", exp: "$0.02", value: currency.Currency(2)},
		{name: "12-cents", exp: "$0.12", value: currency.Currency(12)},
		{name: "123", exp: "$1.23", value: currency.Currency(123)},
		{name: "1234", exp: "$12.34", value: currency.Currency(1234)},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if got, exp := test.value.String(), test.exp; got != exp {
				t.Errorf("unexpected value.  got %q, exp %q", got, test.exp)
			}
		})
	}
}

func TestMarshal(t *testing.T) {
	tests := []struct {
		name  string
		exp   string
		value currency.Currency
	}{
		{name: "zero", exp: `"$0.00"`, value: currency.Currency(0)},
		{name: "2-cents", exp: `"$0.02"`, value: currency.Currency(2)},
		{name: "12-cents", exp: `"$0.12"`, value: currency.Currency(12)},
		{name: "123", exp: `"$1.23"`, value: currency.Currency(123)},
		{name: "1234", exp: `"$12.34"`, value: currency.Currency(1234)},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got, err := json.Marshal(test.value)
			if err != nil {
				t.Errorf("unexpected error marshalling %d: %v", test.value, err)
				return
			}
			if string(got) != test.exp {
				t.Errorf("unexpected value.  got %q, exp %q", string(got), test.exp)
			}
		})
	}
}

func TestUnmarshal(t *testing.T) {
	tests := []struct {
		name string
		exp  currency.Currency
		json string
	}{
		{name: "blank", json: `""`, exp: currency.Currency(0)},
		{name: "dollar-only", json: `"$"`, exp: currency.Currency(0)},
		{name: "zero", json: `"$0.00"`, exp: currency.Currency(0)},
		{name: "2-cents", json: `"$0.02"`, exp: currency.Currency(2)},
		{name: "12-cents", json: `"$0.12"`, exp: currency.Currency(12)},
		{name: "123", json: `"$1.23"`, exp: currency.Currency(123)},
		{name: "1234", json: `"$12.34"`, exp: currency.Currency(1234)},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			var got currency.Currency
			err := json.Unmarshal([]byte(test.json), &got)
			if err != nil {
				t.Errorf("unexpected error unmarshalling %q: %v", test.json, err)
				return
			}
			if got != test.exp {
				t.Errorf("unexpected value.  got %d, exp %d", got, test.exp)
			}
		})
	}

}

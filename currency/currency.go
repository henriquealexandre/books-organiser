package currency

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

type Currency int64

func (c Currency) String() string {
	s := strconv.Itoa(int(c))
	switch len(s) {
	case 0:
		return "$0.00"
	case 1:
		return "$0.0" + s
	case 2:
		return "$0." + s
	default:
		cents := s[len(s)-2:]
		dollars := s[:len(s)-2]
		return "$" + dollars + "." + cents
	}
}

func (c Currency) MarshalJSON() ([]byte, error) {
	s := c.String()
	return json.Marshal(s)
}

func (c *Currency) UnmarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	// see if there was a value...
	if s == "" {
		return nil
	}
	// remove $ if it exists
	if s[0] == '$' {
		s = s[1:]
	}

	if s == "" {
		return nil
	}

	// split values on decimal
	fields := strings.Split(s, ".")
	if len(fields) != 2 {
		return fmt.Errorf("invalid currency format: %q", s)
	}
	value := fields[0] + fields[1]

	// combine and convert back to int
	i, err := strconv.Atoi(value)
	if err != nil {
		return err
	}

	// set actual value to the currency value
	*c = Currency(i)
	return nil
}

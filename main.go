package main

import (
	"log"
	"os"

	"gitlab.com/henriquealexandre/books-organiser/cmd"
)

func main() {
	if err := cmd.Run(os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}

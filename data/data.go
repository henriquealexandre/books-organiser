package data

import (
	"fmt"
	"os"

	// documentation: https://github.com/jmoiron/sqlx
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Service struct {
	db *sqlx.DB
}

type Config struct {
	User             string
	DB               string
	SSLMode          string
	connectionString string
}

func (c *Config) ConnectionString() string {
	if c.connectionString != "" {
		return c.connectionString
	}
	return fmt.Sprintf("user=%s dbname=%s sslmode=%s", c.User, c.DB, c.SSLMode)
}

func NewDevConfig() *Config {
	return &Config{
		User:             "postgres",
		DB:               "books_dev",
		SSLMode:          "disable",
		connectionString: os.Getenv("ESQLBOOKS"),
	}
}

func NewTestConfig() *Config {
	return &Config{
		User:    "foo",
		DB:      "books_test",
		SSLMode: "disable",
	}
}

func New(c *Config) (*Service, error) {
	// check config
	if c == nil {
		/* VSCode says c is never used */
		c = NewDevConfig()
	}

	//opening ElephantSQL connection
	db, err := sqlx.Connect("postgres", c.ConnectionString())
	if err != nil {
		return nil, err
	}

	return &Service{
		db: db,
	}, nil
}

func (s *Service) Initialize() error {
	// create all tables, create any necessary seed data, etc

	var schema = `
	CREATE TABLE IF NOT EXISTS books (
	id SERIAL PRIMARY KEY,
	title VARCHAR(50) NOT NULL,
	author VARCHAR(50),
	comment VARCHAR(50),
	numberOfPages INTEGER,
	publisher VARCHAR(50),
	ISBN VARCHAR(50),
	price DECIMAL,
	translation BOOL,
	edition VARCHAR(99),
	deleted BOOL,
	created_At TIMESTAMP NOT NULL,
	updated_At TIMESTAMP,
	deleted_At TIMESTAMP
);
`
	queryResult, err := s.db.Exec(schema)
	if err != nil {
		return err
	}

	fmt.Println(queryResult)

	return nil

}

func (s *Service) Reset() error {
	result, err := s.db.Exec("DROP DATABASE [IF EXISTS] $1", NewTestConfig().DB)
	if err != nil {
		return err
	}

	fmt.Println("Destructive reset action performed: ", result)

	return nil

}

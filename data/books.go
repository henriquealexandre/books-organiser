package data

import (
	"fmt"
	"log"

	"gitlab.com/henriquealexandre/books-organiser/books"
)

func (s *Service) GetBook(id int) (books.Book, error) {
	// put sql lookup code...
	book := books.Book{}
	err := s.db.Get(&book, "SELECT * FROM books WHERE book_id = $1", id)
	if err != nil {
		return books.Book{}, err
	}
	return book, nil
}

func (s *Service) GetBooks() ([]books.Book, error) {

	books := []books.Book{}

	if err := s.db.Select(&books, "SELECT * FROM books"); err != nil {
		return nil, err
	}

	return books, nil

}

func (s *Service) CreateBook(book *books.Book) error {

	//TODO: Implement basic validation
	/*Should we validate data here or on the form after parsing  request data?*/

	//NamedQuery supports struct
	rows, err := s.db.NamedQuery(`INSERT INTO books(
		book_id
		title
		author
		comment
		numberOfPages
		publisher
		iSBN
		price
		translation
		edition
		created_At
		updated_At
		deleted_At
		) 
		VALUES (
			:ID
			:Title
			:Author
			:Comment
			:NumberOfPages
			:Publisher
			:ISBN
			:Price
			:Translation
			:EditionDate
			) `,
		book)

	if err != nil {
		return err
	}

	for rows.Next() {
		err := rows.StructScan(&book)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Printf("%#v\n", book)
	}
	return nil

}
func (s *Service) Upsert(id int, book *books.Book) error {

	query := `INSERT INTO books (title, author, comment, numberOfPages, publisher, iSBN, price, translation, edition WHERE book_id = ID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`

	result, err := s.db.Exec(query, book.Title, book.Author, book.Comment, book.NumberOfPages, book.Publisher, book.ISBN, book.Price, book.Translation, book.EditionDate, id)

	if err != nil {
		return err
	}

	fmt.Println(result)

	return nil
}

func (s *Service) DelBook(id int) error {

	result, err := s.db.Exec("DELETE FROM books where book_id = $1", id)
	if err != nil {
		return err
	}

	fmt.Println(result)
	return nil
}

package provision

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/henriquealexandre/books-organiser/data"
	"gitlab.com/henriquealexandre/books-organiser/web"
)

func Run(args []string) error {
	var configPath string
	flags := flag.NewFlagSet("command", flag.ContinueOnError)
	flags.StringVar(&configPath, "cfg", "config.toml", "path to config file")
	if err := flags.Parse(args); err != nil {
		return err
	}

	c := &web.Config{}

	if configPath == "" {
		log.Println("no config provided, defaulting to development config")
		c = web.NewDevConfig()
	} else {
		// TODO: actually load the config if they specified it...
	}

	fmt.Println("provisioning database...")
	// Open our DB connection
	db, err := data.New(c.Data)
	if err != nil {
		return err
	}
	err = db.Initialize()
	if err != nil {
		return err
	}
	return nil
}

package cmd

import (
	"fmt"

	"gitlab.com/henriquealexandre/books-organiser/cmd/provision"
	"gitlab.com/henriquealexandre/books-organiser/web"
)

func Run(args []string) error {
	// syntax of commands:
	// books service --cfg=config.toml --host=127.0.0.1 --port=8080
	// books provision

	// check basic arguments
	command := "service"
	if len(args) > 0 {
		command = args[0]
		args = args[1:]
	}

	switch command {
	case "service":
		return web.Run(args)
	case "provision":
		return provision.Run(args)
	default:
		return fmt.Errorf("unknown command %q", command)
	}
}

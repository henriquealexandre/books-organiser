module gitlab.com/henriquealexandre/books-organiser

go 1.15

require (
	github.com/gobuffalo/nulls v0.4.0
	github.com/google/go-cmp v0.5.4
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
)

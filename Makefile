run: build
	go run *.go

deps:
	go get github.com/markbates/refresh

test:
	go test -cover -failfast -race ./...

serve:
	refresh

reload:
	livereload . -ee go,md,mod -x node_modules/ -d 

reflex:
	reflex -- sh ~/reflex.sh

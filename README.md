# README

## Create Database

You will need a `dev` database to work with.  To do this,run the following commands:

```sh
su - postgres
createdb books_dev
```
## Creating Code Coverage

```sh
go test ./books -coverprofile=cover.out
go tool cover -html=cover.out
```

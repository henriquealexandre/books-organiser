package books

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"time"

	"github.com/gobuffalo/nulls"
	"gitlab.com/henriquealexandre/books-organiser/currency"
)

// const layoutISO = "2006-01-02"
// const layoutUS = "January 2, 2006"

var titleRegex = regexp.MustCompile(`^[a-zA-Z0-9 ]+$`)

//Book defines book attributes
type Book struct {
	ID            int               `db:"id"`
	Title         string            `db:"title"`
	Author        string            `db:"author"`
	Comment       string            `db:"comment"`
	NumberOfPages int               `db:"numberofpages"`
	Publisher     string            `db:"publisher"`
	ISBN          int               `db:"isbn"`
	Price         currency.Currency `db:"price"`
	Translation   bool              `db:"translation"`
	//Since EditionDate refers to the year when the book was release, it might be an int. We can also keep time.Time return Year.
	EditionDate int       `db:"edition"`
	Deleted     bool      `db:"deleted"`
	CreatedAt   time.Time `db:"created_at"`
	UpdatedAt   time.Time `db:"updated_at"`
	DeletedAt   time.Time `db:"deleted_at"`
}

type jsonBook struct {
	ID            int               `json:"id"`
	Title         string            `json:"title"`
	Author        nulls.String      `json:"author"`
	Comment       string            `json:"comment"`
	NumberOfPages int               `json:"numpages"`
	Publisher     string            `json:"publisher"`
	ISBN          int               `json:"isbn"`
	Price         currency.Currency `json:"pricecover"`
	Translation   bool              `json:"translation"`
	//Since EditionDate refers to the year when the book was release, it might be an int. We can also keep time.Time return Year.
	EditionDate int `json:"editiondate,omitempty"`
}

func (b Book) MarshalJSON() ([]byte, error) {
	bk := jsonBook{
		ID:            b.ID,
		Title:         b.Title,
		Author:        nulls.NewString(b.Author),
		Comment:       b.Comment,
		NumberOfPages: b.NumberOfPages,
		Publisher:     b.Publisher,
		ISBN:          b.ISBN,
		Price:         b.Price,
		Translation:   b.Translation,
		EditionDate:   b.EditionDate,
	}

	return json.Marshal(bk)
}

func (b *Book) UnmarshalJSON(data []byte) error {
	var bk jsonBook
	err := json.Unmarshal(data, &bk)
	if err != nil {
		return err
	}

	b.ID = bk.ID
	b.Title = bk.Title
	if bk.Author.Valid {
		b.Author = bk.Author.String
	}
	b.Comment = bk.Comment
	b.NumberOfPages = bk.NumberOfPages
	b.ISBN = bk.ISBN
	b.Price = bk.Price
	b.Translation = bk.Translation
	b.EditionDate = bk.EditionDate

	return nil
}

// IsValid validates all fields in the book are correct
func (b Book) IsValid() bool {
	err := b.Validate()
	return err != nil
}

// Validate checks all fields for correctness and returns an error of it
// finds any invalid fields
func (b Book) Validate() error {
	if err := b.ValidID(); err != nil {
		return err
	}

	if err := b.ValidTitle(); err != nil {
		return err
	}

	if err := b.ValidAuthor(); err != nil {
		return err
	}

	if err := b.ValidComment(); err != nil {
		return err
	}

	if err := b.ValidNumberPages(); err != nil {
		return err
	}
	if err := b.ValidPublisher(); err != nil {
		return err
	}

	if err := b.ValidEditionDate(); err != nil {
		return err
	}

	if err := b.ValidPrice(); err != nil {
		return err
	}

	if err := b.ValidISBN(); err != nil {
		return err
	}

	return nil
}

func (b Book) ValidID() error {
	if b.ID <= 0 {
		return fmt.Errorf("This ID of %d is invalid. Please, enter a valid ID.", b.ID)
	}

	return nil
}

func (b Book) ValidTitle() error {
	if len(b.Title) <= 1 {
		return errors.New("Title is too short. Please, enter a valid title name")
	}

	if len(b.Title) >= 100 {
		return errors.New("Title is too long. Please, enter a valid title name")
	}

	if !titleRegex.MatchString(b.Title) {
		return errors.New("Make sure the title has no special characters")
	}

	return nil
}

func (b Book) ValidAuthor() error {
	if len(b.Author) > 25 {
		return errors.New("Name is too long. Please, dry it up")
	}
	if len(b.Author) < 5 {
		return errors.New("Name is too short. Please, give more info")
	}

	if !titleRegex.MatchString(b.Author) {
		return errors.New("Make sure the title has no special characters")
	}

	return nil
}

func (b Book) ValidComment() error {
	if len(b.Comment) > 200 {
		return errors.New("Comment is too long. Please dry it up")
	}

	if len(b.Comment) < 20 {
		return fmt.Errorf("%d characters is too short of a comment. Please, be more specific", len(b.Comment))
	}

	if !titleRegex.MatchString(b.Comment) {
		return errors.New("Make sure the title has no special characters")
	}

	return nil
}

func (b Book) ValidNumberPages() error {
	if b.NumberOfPages <= 0 {
		return errors.New("invalid number of pages")
	}

	return nil
}

// TODO: Create test cases
func (b Book) ValidPublisher() error {

	if len(b.Publisher) <= 2 {
		return errors.New("Title is too short. Please, enter a valid title name")
	}

	if len(b.Publisher) > 25 {
		return errors.New("Title is too long. Please, enter a valid title name")
	}

	if titleRegex.MatchString(b.Title) {
		return errors.New("Make sure the title has no special characters")
	}

	return nil
}

// TODO: Create test cases
func (b Book) ValidISBN() error {

	s := strconv.Itoa(b.ISBN)

	if len(s) < 10 || len(s) > 13 {
		return errors.New("invalid ISBN number. Please, review it and try again")
	}

	return nil
}

func (b Book) ValidPrice() error {
	if b.Price <= 0 {
		return errors.New("Invalid price. Please, review it and try again")
	}
	return nil
}

func (b Book) ValidEditionDate() error {

	if b.EditionDate > time.Now().Year() {
		return errors.New("Future dates are not accepted as valid. Please, submit it again")
	}

	return nil
}

package books_test

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/henriquealexandre/books-organiser/books"
	"gitlab.com/henriquealexandre/books-organiser/currency"
)

func TestJSONInOut(t *testing.T) {
	t.Skip("come back and update this after validation tests pass")

	// exp stands for "expected", as in "expected outcome"
	exp := books.Book{
		ID:            001,
		Title:         "Moby Dick",
		Author:        "Herman Melville",
		ISBN:          187464955610,
		Translation:   false,
		NumberOfPages: 1,
		//Adding Year
		EditionDate: 2006,
	}

	b, err := json.Marshal(exp)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(b))

	// got stands for "what did I get when I ran the test scenario"
	got := books.Book{}

	if err := json.Unmarshal(b, &got); err != nil {
		t.Fatal(err)
	}

	//if book.ID != book2.ID {
	//t.Errorf("unepxected ID: got: %d, exp %d", book.ID, book2.ID)
	//}

	if !cmp.Equal(got, exp) {
		t.Errorf("mismatched values for book:\n%s", cmp.Diff(got, exp))
	}
}

func TestJSONGoodData(t *testing.T) {
	t.Skip("fix this test once we finish validation tests")
	tests := []struct {
		name string
		exp  books.Book
		data []byte
	}{
		{
			name: "test price",
			exp:  books.Book{ID: 10, Price: 3333},
			data: []byte(`{"id":10, "pricecover":"$33.33"}`),
		},
		{
			name: "test time",
			exp:  books.Book{ID: 10, EditionDate: 2021},
			data: []byte(`{"id":10, "editiondate":"2021-03-04"}`),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := books.Book{}

			if err := json.Unmarshal(test.data, &got); err != nil {
				t.Error(err)
			}
			t.Logf("%+v", got)

			if !cmp.Equal(got, test.exp) {
				t.Errorf("mismatched values for book:\n%s", cmp.Diff(got, test.exp))
			}
		})
	}
}

// TODO: TestMarshal

func TestUnmarshal(t *testing.T) {
	t.Skip("fix this once validation tests are working")
	tests := []struct {
		name string
		exp  books.Book
		data []byte
	}{
		// TODO: Test all fields (add more scenarios)

		{
			name: "test author",
			exp:  books.Book{ID: 10, Author: "Bob", Title: "Go"},
			data: []byte(`{"id":10, "title":"Go"}`),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := books.Book{}
			got.ID = test.exp.ID
			got.Author = test.exp.Author
			got.Title = test.exp.Title

			if err := json.Unmarshal(test.data, &got); err != nil {
				t.Error(err)
			}
			t.Logf("%+v", got)

			if !cmp.Equal(got, test.exp) {
				t.Errorf("mismatched values for book:\n%s", cmp.Diff(got, test.exp))
			}
		})
	}
}

func TestJSONBadData(t *testing.T) {
	data := []byte(`{"id":10, "pricecover":"-"}`)
	got := books.Book{}

	if err := json.Unmarshal(data, &got); err == nil {
		t.Errorf("expected an error")
	}

	t.Logf("%+v", got)
}

func TestJSONMoney(t *testing.T) {
	data := books.Book{
		Title: "The Great Gatsby",
		Price: 15,
	}

	byteJson, err := json.Marshal(data)
	if err != nil {
		t.Errorf("something went wrong: 	%v", err)
	}

	mJson, err := json.MarshalIndent(byteJson, "", " ")
	if err != nil {
		t.Errorf("something went wrong: 	%v", err)
	}

	t.Logf("%+v", string(mJson))

}

func TestValidID(t *testing.T) {
	tests := []struct {
		name   string
		ID     int
		expErr bool
	}{
		{name: "zero", ID: 0, expErr: true},
		{name: "one", ID: 1},
		{name: "negative", ID: -1, expErr: true},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			b := books.Book{
				ID: test.ID,
			}
			err := b.ValidID()
			if err != nil && !test.expErr {
				t.Errorf("something went wrong: %v", err)
				return
			}
			if err == nil && test.expErr {
				t.Error("expected error, got none")
				return
			}
		})
	}
}

func TestValidTitle(t *testing.T) {
	tests := []struct {
		name   string
		Title  string
		expErr bool
	}{
		{name: "first", Title: "X", expErr: true},
		{name: "second", Title: `Then she is led by the ghost of the Fox into a sunlit arena in which she learns the story of what Psyche has been up to: 
		she has herself been assigned the impossible tasks from Orual's dreams, but was able to complete them with supernatural help. Orual then leaves the arena...`, expErr: true},
		{name: "third", Title: "This is a normal title"},
		{name: "fourth", Title: "$p£ci@| Ch@r!", expErr: true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			b := books.Book{
				Title: test.Title,
			}
			t.Logf("%+v", b)
			err := b.ValidTitle()
			if err != nil && !test.expErr {
				t.Errorf("something went wrong: %v", err)
				return
			}
			if err == nil && test.expErr {
				t.Errorf("expected error, got nothing")
				return
			}
		})
	}
}

func TestValidAuthor(t *testing.T) {
	tests := []struct {
		name   string
		Author string
		expErr bool
	}{
		{name: "first", Author: "H3rm@n", expErr: true},
		{name: "second", Author: "we", expErr: true},
		{name: "third", Author: "testing any name"},
		{name: "fourth", Author: "The Herman Melville family of american writers", expErr: true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			b := books.Book{
				Author: test.Author,
			}

			err := b.ValidAuthor()
			if err != nil && !test.expErr {
				t.Errorf("something went wrong: %v", err)
				return
			}
			if err == nil && test.expErr {
				t.Errorf("expected error, got nothing")
				return
			}

		})

	}
}

func TestValidComment(t *testing.T) {

	tests := []struct {
		name    string
		Comment string
		expErr  bool
	}{
		{name: "first", Comment: "Narnia is a book written by CS Lewis"},
		{name: "second", Comment: `[The universe] may be full of lives that have been redeemed in modes suitable to their condition, 
		of which we can form no conception. It may be full of lives that have been redeemed in the very same mode as our own. 
		It may be full of things quite other than life in which God is interested though we are not.`, expErr: true},
		{name: "third", Comment: "Th!$ C0mm£nt", expErr: true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			b := books.Book{
				Comment: test.Comment,
			}

			err := b.ValidComment()
			if err != nil && !test.expErr {
				t.Errorf("something went wrong: %v", err)
				return
			}
			if err == nil && test.expErr {
				t.Errorf("expected error, got nothing")
				return
			}

		})

	}
}
func TestValidNumberPages(t *testing.T) {
	tests := []struct {
		name        string
		NumberPages int
		expErr      bool
	}{
		{name: "zero", NumberPages: 0, expErr: true},
		{name: "positive", NumberPages: 200},
		{name: "negative", NumberPages: -20, expErr: true},
		{name: "one", NumberPages: 1},
		{name: "negative-one", NumberPages: -1, expErr: true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			b := books.Book{
				NumberOfPages: test.NumberPages,
			}
			err := b.ValidNumberPages()
			if err != nil && !test.expErr {
				t.Errorf("something went wrong: %v", err)
				return
			}
			if err == nil && test.expErr {
				t.Errorf("expected error, got nothing")
				return
			}

		})
	}
}

func TestValidPublisher(t *testing.T) {
	tests := []struct {
		name   string
		Author string
		expErr bool
	}{
		{name: "first", Author: "H3rm@n", expErr: true},
		{name: "second", Author: "we", expErr: true},
		{name: "third", Author: "testing any name"},
		{name: "fourth", Author: "The Herman Melville family of american writers", expErr: true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			b := books.Book{
				Author: test.Author,
			}

			err := b.ValidAuthor()
			if err != nil && !test.expErr {
				t.Errorf("something went wrong: %v", err)
				return
			}
			if err == nil && test.expErr {
				t.Errorf("expected error, got nothing")
				return
			}

		})

	}
}

func TestValidISBN(t *testing.T) {

	tests := []struct {
		name   string
		ISBN   int
		expErr bool
	}{
		{name: "first", ISBN: 12345, expErr: true},
		{name: "second", ISBN: 1234598760},
		{name: "third", ISBN: 1413121110123},
		{name: "third", ISBN: 2468246824682468246, expErr: true},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			b := books.Book{
				ISBN: test.ISBN,
			}

			err := b.ValidISBN()

			if err != nil && !test.expErr {
				t.Errorf("something went wrong: %v", err)
				return
			}
			if err == nil && test.expErr {
				t.Errorf("expected error, got nothing")
				return
			}

		})
	}
}

func TestValidPrice(t *testing.T) {
	tests := []struct {
		name   string
		Price  currency.Currency
		expErr bool
	}{
		{name: "negative", Price: -200, expErr: true},
		{name: "zero", Price: 0, expErr: true},
		{name: "positive", Price: 50},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			b := books.Book{
				Price: test.Price,
			}

			err := b.ValidPrice()

			if err != nil && !test.expErr {
				t.Errorf("something went wrong: %v", err)
				return
			}
			if err == nil && test.expErr {
				t.Errorf("expected error, got nothing")
				return
			}

		})
	}

}

func TestValidEditionDate(t *testing.T) {

	d1 := time.Now().Year()
	d2 := 2018
	d3 := time.Now().Year() + 1

	tests := []struct {
		name   string
		Date   int
		expErr bool
	}{
		{name: "past", Date: d1},
		{name: "present", Date: d2},
		{name: "future", Date: d3, expErr: true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			b := books.Book{
				EditionDate: test.Date,
			}

			err := b.ValidEditionDate()
			if err != nil && !test.expErr {
				t.Errorf("something went wrong: %v", err)
				return
			}
			if err == nil && test.expErr {
				t.Errorf("expected error, got nothing")
				return
			}

		})

	}

}

package web

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/henriquealexandre/books-organiser/books"
	"gitlab.com/henriquealexandre/books-organiser/data"
)

type Service struct {
	router *mux.Router
	DB     interface {
		GetBooks() ([]books.Book, error)
		GetBook(int) (books.Book, error)
		Upsert(int, *books.Book) error
		DelBook(int) error
	}
}

var newb books.Book

func NewService(c *Config) (*Service, error) {
	if c == nil {
		c = NewDevConfig()
	}

	router := mux.NewRouter()

	s := &Service{}

	// Open our DB connection
	db, err := data.New(c.Data)
	if err != nil {
		return nil, err
	}
	s.DB = db

	// define our routes
	router.HandleFunc("/", s.HomePage).Methods("GET")
	router.HandleFunc("/books", s.ListBooks).Methods("GET")
	router.HandleFunc("/books/new", s.NewBook).Methods("POST")
	router.HandleFunc("/books/{id}", s.GetBook).Methods("GET")
	router.HandleFunc("/books/{id}", s.UpdateBook).Methods("POST")
	router.HandleFunc("/books/{id}", s.DeleteBook).Methods("DELETE")

	// Path Prefix is the path we want to match: http://localhost:8080/faq
	// We need to strip the prefix of `faq` from the file server, or it will try to route to:
	//    http://localhost:8080/faq/ui/Maxim/index.html
	// and we want it to route to
	//    http://localhost:8080/ui/Maxim/index.html

	router.PathPrefix("/faq/").Handler(http.StripPrefix("/faq/", http.FileServer(http.Dir("./ui/Maxim/"))))

	s.router = router
	// TODO: Enable this to only run if debug is turned on

	if c.Debug {
		printRoutes(s.router)
	}

	return s, nil
}

func printRoutes(router *mux.Router) {
	router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		t, err := route.GetPathTemplate()
		if err != nil {
			log.Println(err)
			return err
		}
		fmt.Println(strings.Repeat("| ", len(ancestors)), t)
		return nil
	})
}

// Handle all routing for service
func (s *Service) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *Service) HomePage(w http.ResponseWriter, r *http.Request) {
	booksList, err := s.DB.GetBooks()
	if err != nil {
		log.Println(err)
	}
	var names []string
	for _, book := range booksList {
		names = append(names, book.Title)
	}

	byteNames, err := json.MarshalIndent(names, "", " ")
	if err != nil {
		fmt.Fprint(w, err)
	}

	fmt.Fprint(w, string(byteNames))
}

func (s *Service) ListBooks(w http.ResponseWriter, r *http.Request) {
	//err := s.DB.Initialize()
	//if err != nil {
	//log.Println(err)
	//http.Error(w, "unable to initalize books", http.StatusInternalServerError)
	//return
	//}
	bks, err := s.DB.GetBooks()
	if err != nil {
		log.Println(err)
		http.Error(w, "unable to retrieve books", http.StatusInternalServerError)
		return
	}
	// create some books and send them back

	if err := json.NewEncoder(w).Encode(bks); err != nil {
		http.Error(w, "unable to encode json", http.StatusInternalServerError)
	}
}

func (s *Service) NewBook(w http.ResponseWriter, r *http.Request) {
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(100)

	bk := books.Book{
		ID:          n,
		Title:       "Time Machine",
		Author:      "H.G Wells",
		Price:       50,
		Translation: false,
	}
	if err := json.NewEncoder(w).Encode(bk); err != nil {
		http.Error(w, "unable to encode json", http.StatusInternalServerError)
	}

	newb = bk
}

func (s *Service) GetBook(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "invalid id", http.StatusBadRequest)
		return
	}
	log.Println(err)

	book, err := s.DB.GetBook(id)
	// TODO: detect a not found vs general server error and return appropriate status
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "\t")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if encoder.Encode(book); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Service) UpdateBook(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "invalid id", http.StatusInternalServerError)
		return
	}
	log.Println(err)

	book, err := s.DB.GetBook(id)
	if err != nil {
		http.Error(w, "sorry, something went wrong", http.StatusInternalServerError)
		return
	}

	err = s.DB.Upsert(id, &book)
	if err != nil {
		http.Error(w, "sorry, something went wrong", http.StatusInternalServerError)
	}

	http.Redirect(w, r, "/", http.StatusOK)
	// decode from r.Body
	// if err := json.NewDecoder(r.Body).Decode(&b); err != nil {
	// 	http.Error(w, "unable to Decode json", http.StatusInternalServerError)
	// }
	// defer r.Body.Close()

}

func (s *Service) DeleteBook(w http.ResponseWriter, r *http.Request) {
	//newb.Deleted = true
	fmt.Fprintf(w, "Book %s deleted", newb.Title)

	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "invalid id", http.StatusInternalServerError)
	}
	log.Println(err)

	err = s.DB.DelBook(id)
	if err != nil {
		http.Error(w, "sorry, something went wrong", http.StatusInternalServerError)

		fmt.Fprintf(w, "book %v deleted", id)
	}
}

// Use the following command to test for a "valid" book:
// TODO: Finish filling in JSON to create a valid curl command
/*
   curl --header "Content-Type: application/json" \
     --request POST \
   	--data '{"id":30,"title":"Go Is Awesome","author":"Jenny Gopher","comment":"Really good book to read","numpages":500}' \
   	http://localhost:8080/books/30
*/

// Use the following command to test for an "invalid" book:

// TODO: Write some unit tests..

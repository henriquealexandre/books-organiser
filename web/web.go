package web

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
)

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, World!")
}

type logWriter struct {
	*log.Logger
}

func (l *logWriter) Write(p []byte) (int, error) {
	l.Println(string(p))
	return len(p), nil
}

func Run(args []string) error {
	var configPath string
	var host string
	var port string

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	flags := flag.NewFlagSet("command", flag.ContinueOnError)
	flags.StringVar(&configPath, "cfg", "config.toml", "path to config file")
	flags.StringVar(&host, "host", "localhost", "host to start up service on")
	flags.StringVar(&port, "port", "8080", "port to start up service on")
	if err := flags.Parse(args); err != nil {
		return err
	}

	hp := net.JoinHostPort(host, port)

	c, err := LoadConfig(configPath)
	if err != nil {
		return err
	}

	h, err := NewService(c)
	if err != nil {
		return err
	}

	s := handlers.CombinedLoggingHandler(&logWriter{Logger: infoLog}, h)

	infoLog.Printf("Starting server on http://%s", hp)

	server := &http.Server{
		Addr:     hp,
		Handler:  s,
		ErrorLog: errorLog,
	}

	errorLog.Fatal(server.ListenAndServe())
	return nil
}

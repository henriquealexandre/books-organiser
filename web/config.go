package web

import (
	"gitlab.com/henriquealexandre/books-organiser/data"
)

func LoadConfig(path string) (*Config, error) {
	return NewDevConfig(), nil

	// TODO: load config from file...

	//if path == "" {
	//log.Println("no config provided, defaulting to development config")
	//return NewDevConfig(), nil
	//}

	//return &Config{}, nil
}

func NewDevConfig() *Config {
	return &Config{
		Data:  data.NewDevConfig(),
		Debug: true,
	}
}

func NewTestConfig() *Config {
	return &Config{
		Data:  data.NewTestConfig(),
		Debug: true,
	}
}

type Config struct {
	Data  *data.Config
	Debug bool
}

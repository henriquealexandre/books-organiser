package web_test

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/henriquealexandre/books-organiser/books"
	"gitlab.com/henriquealexandre/books-organiser/web"
)

func Test_HomePage(t *testing.T) {

	s := &web.Service{
		DB: &dbStub{},
	}

	request := httptest.NewRequest("GET", "/", nil)
	responser := httptest.NewRecorder()

	s.HomePage(responser, request)

	if got, exp := responser.Code, http.StatusOK; got != exp {
		t.Errorf("Unexpected response code. Got %d, was expecting %d\n", got, exp)
	}
	if got, exp := responser.Body.String(), "<h1>Welcome to our awesome site!</h1>"; got != exp {
		t.Errorf("Unexpected body. Got %s, was expecting %s \n", got, exp)
	}

}

func TestService(t *testing.T) {
	s, err := web.NewService(web.NewDevConfig())
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()

	s.ServeHTTP(w, r)

	if w.Code != http.StatusOK {
		t.Fatal("hmmm")
	}
	t.Log(string(w.Body.String()))
}

func Test_GetBook(t *testing.T) {

	s, err := web.NewService(web.NewDevConfig())
	if err != nil {
		log.Println(err)
	}

	request := httptest.NewRequest("GET", "/books/02", nil)
	responser := httptest.NewRecorder()

	s.ServeHTTP(responser, request)

	if got, exp := responser.Code, http.StatusOK; got != exp {
		t.Errorf("Unexpected response code. Got %d, was expecting %d\n", got, exp)
	}

	if got, exp := responser.Body.String(), "you are on the GetBook endpoint \"02\""; got != exp {
		t.Errorf("Unexpected item. Got %q, was expecting %q\n", got, exp)
	}

}

func Test_ListBooks(t *testing.T) {
	s := &web.Service{
		DB: &dbStub{},
	}

	request := httptest.NewRequest("GET", "/books", nil)
	responser := httptest.NewRecorder()

	exp := []books.Book{
		{
			ID:    1,
			Title: "Go is awesome",
		},
		{
			ID:    2,
			Title: "Advanced Go",
		},
	}

	s.ListBooks(responser, request)

	if got, exp := responser.Code, http.StatusOK; got != exp {
		t.Errorf("Unexpected response code. Got %d, was expecting %d\n", got, exp)
	}

	got := []books.Book{}
	t.Logf("response:\n%s\n", responser.Body.String())
	err := json.Unmarshal(responser.Body.Bytes(), &got)
	if err != nil {
		t.Fatal(err)
	}
	if !cmp.Equal(got, exp) {
		t.Errorf("There was some problem.\n%s\n", cmp.Diff(got, exp))
	}

}

type dbStub struct {
}

func (d *dbStub) GetBooks() ([]books.Book, error) {
	return nil, nil
}

func (d *dbStub) GetBook(int) (books.Book, error) { return books.Book{}, nil }
func (d *dbStub) Upsert(int, *books.Book) error   { return nil }
func (d *dbStub) DelBook(int) error               { return nil }
